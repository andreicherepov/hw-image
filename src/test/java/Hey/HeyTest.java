package Hey;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class HeyTest {

    @Test
    public void TestHey() {
        assertEquals("Hey!", Hey.SayHey());
    }

}
